<?php
require("vendor/autoload.php");

for ($i = 0; $i < 2; $i++) {
    foreach (glob(__DIR__.'/src/*.php') as $path) {
        $className = "Charm\\".substr(basename($path), 0, -4);
        try {
            echo "Throwing: $className... ";
            throw new $className('Some Message');
        } catch (\Throwable $e) {
            if ($e::class !== $className) {
                throw $e;
            }
            echo "SUCCESS\n";
            echo " - message:       ".$e->getMessage()."\n";
            echo " - code:          ".$e->getCode()."\n";
            echo " - line:          ".$e->getLine()."\n";
            echo " - file:          ".$e->getFile()."\n";
            echo " - httpCode:      ".$e->getHttpCode()."\n";
            echo " - reasonPhrase:  ".$e->getReasonPhrase()."\n";
            echo " - JSON:\n".json_encode($e, \JSON_PRETTY_PRINT)."\n";
            echo "\n";
        }
    }
    !defined('DEBUG') && define('DEBUG', true);
}
