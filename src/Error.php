<?php
declare(strict_types=1);

namespace Charm;

/**
 * Exceptions might not be errors. They may simply be exceptions. That is why
 * we have both Error and Exception.
 *
 * All Error are Exception, but not all Exception are Error.
 */
class Error extends Exception {
}