<?php
namespace Charm;

use Charm\Error\ExceptionTrait;
use Charm\Error\ExceptionInterface;

class LogicError extends \LogicException implements ExceptionInterface {
    use ExceptionTrait;

    public function getReasonPhrase(): string
    {
        return $this->reasonPhrase ?? 'Internal Logic Error';
    }
}