<?php
declare(strict_types=1);

namespace Charm\Error;

use JsonSerializable;

interface ExceptionInterface extends JsonSerializable
{
    /**
     * The HTTP status code to use, if this error is unhandled.
     * 
     * @return int
     */
    public function getHttpCode();
    public function setHttpCode(int $code): self;

    /**
     * The HTTP status message to use, if this error is unhandled.
     * 
     * @return string
     */
    public function getReasonPhrase();
    public function setReasonPhrase(string $reasonPhrase): self;

    /**
     * Return any additional error information
     *
     * @return array
     */
    public function getExtra();
    public function setExtra(array $extra): self;
    
    /**
     * In some cases we allow error codes to be provided as
     * strings. The PHP Exception class does not support strings,
     * so we use this method to make the code available.
     *
     * @return mixed
     */
    public function getOriginalErrorCode();

    /**
     * Some exceptional circumstances involve sending headers to
     * the browser. One example is the 'Location' header, which
     * should be added for a 3XX redirect exception.
     *
     * @return array
     */
    public function getHeaders();
    public function addHeader(string $header, string $value): self;

    /**
     * This function must be implemented for security purposes
     *
     * @return array
     */
    public function __debugInfo();

    /**
     * This function is required for conveying error messages
     * via APIs.
     *
     * @return void
     */
    public function jsonSerialize();
}
