<?php
declare(strict_types=1);

namespace Charm\Error;

/**
 * This trait makes exceptions comply with
 * Charm\Interfaces\ExceptionInterface.
 */
trait ExceptionTrait
{
    /**
     * Extra info may be added via functions only. This is because we want to
     * allow the extraInfo array to have a predictable format.
     */
    private array $extra = [];

    /**
     * HTTP status code.
     */
    private ?int $httpCode = null;

    /**
     * HTTP reason phrase.
     */
    private ?string $reasonPhrase = null;

    public function getHttpCode(): int
    {
        if (null === $this->httpCode) {
            if (isset(HttpCodes::PHRASES[$this->getCode()])) {
                return $this->getCode();
            } else {
                return 500;
            }
        }

        return $this->httpCode;
    }

    public function setHttpCode(int $code): self
    {
        $this->httpCode = $code;

        return $this;
    }

    public function getReasonPhrase(): string
    {
        if (null === $this->reasonPhrase) {
            return HttpCodes::PHRASES[$this->getHttpCode()] ?? 'Internal Server Error';
        }

        return $this->reasonPhrase;
    }

    public function setReasonPhrase(string $reasonPhrase): self
    {
        $this->reasonPhrase = $reasonPhrase;

        return $this;
    }

    public function getExtra(): array
    {
        return $this->extra;
    }

    public function setExtra(array $extra): self
    {
        if (\array_key_exists(0, $extra)) {
            // An explicit HTTP error code was provided
            $this->setHttpCode($extra[0]);
            unset($extra[0]);
        }

        if (\array_key_exists(1, $extra)) {
            $this->setReasonPhrase($extra[1]);
            unset($extra[1]);
        }

        $this->extra = $extra;

        return $this;
    }

    public function getOriginalErrorCode(): mixed
    {
        return $this->extra['originalErrorCode'] ?? $this->getCode();
    }

    public function setOriginalErrorCode(mixed $code): self
    {
        $this->extra['originalErrorCode'] = $code;

        return $this;
    }

    public function getHeaders(): array
    {
        return $this->extra['headers'] ?? [];
    }

    public function addHeader(string $header, string $value): self
    {
        $this->extra['headers'][$header][] = $value;

        return $this;
    }

    final public function jsonSerialize()
    {
        $result = [
            'error' => [
                'message' => $this->getMessage(),
                'code' => $this->getCode(),
            ],
            'httpStatus' => [
                'code' => $this->getHttpCode(),
                'reason' => $this->getReasonPhrase(),
            ],
        ];

        if (\defined('DEBUG') && \constant('DEBUG')) {
            $result['DEBUG_IS_ENABLED'] = [
                'type' => static::class,
                'file' => $this->getFile(),
                'line' => $this->getLine(),
                'trace' => $this->getTrace(),
                'previous' => $this->getPrevious(),
            ];
            if ([] !== $this->extra) {
                $result['DEBUG_IS_ENABLED']['extraInfo'] = $this->extra;
            }
        }

        return $result;
    }

    public function __debugInfo()
    {
        return $this->jsonSerialize();
    }
}
