<?php
namespace Charm\Error;

interface HttpCodes {
    /**
     * This was copied from nyholm/psr-7 because the const was private.
     *
     * @var array map of standard HTTP status code/reason phrases
     * */
    public const PHRASES = [
        100 => 'Continue', 101 => 'Switching Protocols', 102 => 'Processing',
        200 => 'OK', 201 => 'Created', 202 => 'Accepted', 203 => 'Non-Authoritative Information', 204 => 'No Content', 205 => 'Reset Content', 206 => 'Partial Content', 207 => 'Multi-status', 208 => 'Already Reported',
        300 => 'Multiple Choices', 301 => 'Moved Permanently', 302 => 'Found', 303 => 'See Other', 304 => 'Not Modified', 305 => 'Use Proxy', 306 => 'Switch Proxy', 307 => 'Temporary Redirect',
        400 => 'Bad Request', 401 => 'Unauthorized', 402 => 'Payment Required', 403 => 'Forbidden', 404 => 'Not Found', 405 => 'Method Not Allowed', 406 => 'Not Acceptable', 407 => 'Proxy Authentication Required', 408 => 'Request Time-out', 409 => 'Conflict', 410 => 'Gone', 411 => 'Length Required', 412 => 'Precondition Failed', 413 => 'Request Entity Too Large', 414 => 'Request-URI Too Large', 415 => 'Unsupported Media Type', 416 => 'Requested range not satisfiable', 417 => 'Expectation Failed', 418 => 'I\'m a teapot', 422 => 'Unprocessable Entity', 423 => 'Locked', 424 => 'Failed Dependency', 425 => 'Unordered Collection', 426 => 'Upgrade Required', 428 => 'Precondition Required', 429 => 'Too Many Requests', 431 => 'Request Header Fields Too Large', 451 => 'Unavailable For Legal Reasons',
        500 => 'Internal Server Error', 501 => 'Not Implemented', 502 => 'Bad Gateway', 503 => 'Service Unavailable', 504 => 'Gateway Time-out', 505 => 'HTTP Version not supported', 506 => 'Variant Also Negotiates', 507 => 'Insufficient Storage', 508 => 'Loop Detected', 511 => 'Network Authentication Required',
    ];

    /**
     * Error code descriptions can be found at https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
     */

    /**
     * The request has more than one possible response. The user-agent or user should choose one 
     * of them. (There is no standardized way of choosing one of the responses, but HTML links to 
     * the possibilities are recommended so the user can pick.)
     */
    public const MULTIPLE_CHOICE = 300;
    /**
     * The URL of the requested resource has been changed permanently. The new URL is given in the response.
     */
    public const MOVED_PERMANENTLY = 301;
    /**
     * This response code means that the URI of requested resource has been changed temporarily. 
     * Further changes in the URI might be made in the future. Therefore, this same URI should 
     * be used by the client in future requests.
     */
    public const FOUND = 302;
    /**
     * The server sent this response to direct the client to get the requested resource at another 
     * URI with a GET request.
     */
    public const SEE_OTHER = 303;
    /**
     * This is used for caching purposes. It tells the client that the response has not been modified,
     * so the client can continue to use the same cached version of the response.
     */
    public const NOT_MODIFIED = 304;
    /**
     * The server sends this response to direct the client to get the requested resource at another 
     * URI with same method that was used in the prior request. This has the same semantics as the 
     * 302 Found HTTP response code, with the exception that the user agent must not change the 
     * HTTP method used: If a POST was used in the first request, a POST must be used in the second 
     * request.
     */
    public const TEMPORARY_REDIRECT = 307;
    /**
     * This means that the resource is now permanently located at another URI, specified by the 
     * Location: HTTP Response header. This has the same semantics as the 301 Moved Permanently 
     * HTTP response code, with the exception that the user agent must not change the HTTP method 
     * used: If a POST was used in the first request, a POST must be used in the second request.
     */
    public const PERMANENT_REDIRECT = 308;
    /**
     * When the request is incomplete or invalid
     */
    public const BAD_REQUEST = 400;
    /**
     * When the user is not authenticated and should authenticate
     */
    public const UNAUTHORIZED = 401;
    /**
     * When the user is authenticated, but the request is denied due to lack of privileges
     */
    public const FORBIDDEN = 403;
    /**
     * When the resource or a resource that was required to complete the request was not found.
     */
    public const NOT_FOUND = 404;
    /**
     * When the endpoint does not support the HTTP method used.
     */
    public const METHOD_NOT_ALLOWED = 405;
    /**
     * When the request can't be processed because of unsupported content negotiation
     */
    public const NOT_ACCEPTABLE = 406;
    /**
     * When the request conflicts with the current state of the server
     */
    public const CONFLICT = 409;
    /**
     * Any internal error
     */
    public const INTERNAL = 500;
    public const NOT_IMPLEMENTED = 501;
    public const SERVICE_UNAVAILABLE = 503;
    /**
     * This error response means that the server, while working as a gateway to get a 
     * response needed to handle the request, got an invalid response.
     */
    public const BAD_GATEWAY = 502;
    /**
     * This error response is given when the server is acting as a gateway and cannot
     * get a response in time.
     */
    public const GATEWAY_TIMEOUT = 504;
    public const INSUFFICIENT_STORAGE = 507;
}