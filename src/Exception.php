<?php
declare(strict_types=1);

namespace Charm;

use Throwable;
use JsonSerializable;
use Charm\Error\HttpCodes;
use Charm\Error\ExceptionTrait;
use Charm\Error\ExceptionInterface;

/**
 * An Exception is an exception to what's normal. That does not mean
 * that it is an error. Please extend Error if the event that is happening,
 * is an Error, and not just a result that is not the normal result.
 *
 * For example 10 divided by 0 is an Error. It is not just exceptional,
 * unless we're being irrational.
 *
 * Being out of memory is not an error, it is an exception because we
 * did everything right.
 *
 * It may be hard to know what is right, so when in doubt - use
 * Exception, and improve it to Error later. It is harder to go back
 * to Exception later, because other code may be catching Error and will
 * miss the Exception.
 *
 * Finally; as a rule of thumb - either use HTTP Status Codes as error codes,
 * or provide HTTP Status Codes in the $extraInfo parameter, i.e.:
 *
 * - When something wasn't found, 404 is a good number.
 * - When access was denied, 403 is a good number.
 * - Easy to remember, easy to look up.
 */
class Exception extends \Exception implements ExceptionInterface, JsonSerializable
{
    use ExceptionTrait;

    public function __construct(string $message = '', mixed $code = 0, Throwable $previous = null, array $extraInfo = [])
    {
        parent::__construct($message, (int) $code, $previous);

        if (!is_numeric($code)) {
            $this->setOriginalErrorCode($code);
        }

        $this->setExtra($extraInfo);
    }

    
}
