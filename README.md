charm/error
===========

Just an interface, a trait, an exception class and an error class. They are the
ancestors of all other exceptions in the Charm\ namespace.

The exception interfaces are API oriented (they extend \JsonSerializable) and provide
a default HTTP status code and message - which is the reason they are shared between
other Charm\ libraries.
